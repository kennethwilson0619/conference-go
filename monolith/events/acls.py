import requests
import json
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    iso_code = "ISO 3166-2:US"
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{iso_code}&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    request = requests.get(geocode_url)
    response = json.loads(request.content)
    lat = response[0]["lat"]
    lon = response[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_request = requests.get(weather_url)
    weather_response = json.loads(weather_request.content)
    weather_dict = {
        "temperature": weather_response["main"]["temp"],
        "description": weather_response["weather"][0]["description"],
    }
    try:
        return weather_dict
    except:
        {"weather": None}
    

