from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
import datetime 

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_accounts_vo(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.isoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            defaults={
                "first_name": first_name,
                "last_name": last_name,
                "updated": updated,
            },
        )
    else:
        AccountVO.objects.filter(email=email).delete()



def main():
    while True:
        try:
            # Set the hostname that we'll connect to
            parameters = pika.ConnectionParameters(host="rabbitmq")
            # Create a connection to RabbitMQ
            connection = pika.BlockingConnection(parameters)
            # Open a channel to RabbitMQ
            channel = connection.channel()
            # Create a queue if it does not exist
            channel.exchange_declare(exchange='account_info', exchange_type='fanout')

            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue
            print(queue_name)
            channel.queue_bind(exchange='account_info', queue=queue_name)
            # Configure the consumer to call the process_message function
            # when a message arrives
            channel.basic_consume(
                queue=" ",
                on_message_callback=update_accounts_vo,
                auto_ack=True,
            )
            # Print a status
            print(" [*] Waiting for messages. To exit press CTRL+C")
            # Tell RabbitMQ that you're ready to receive messages
            channel.start_consuming()
        #If the connection fails, then it will print out a message and go to sleep for 2 seconds and it will execute again.
        except AMQPConnectionError:
            print("Could not connect to RabbitMQ")
            time.sleep(2.0)
            
            
            # channel.queue_declare(queue="presentation_approvals")
            # channel.queue_declare(queue="presentation_rejections")